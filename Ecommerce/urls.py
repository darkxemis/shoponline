from django.urls import path
from Ecommerce import views
from django.conf.urls import url

app_name='Ecommerce'

urlpatterns = [
	url(r'^(?P<slug>[\w-]+)/$', views.dispatch_slug, name="dispatch_slug"),
	path('add_product_cart/', views.add_product_cart, name='add_product_cart'),
]