from django.shortcuts import render, get_object_or_404, redirect
#importar solo modelos que vayan a usarse.
from .models import Category, Product, Slug
from CMS.models import SocialLink

def dispatch_slug(request, slug):
    categories = Category.objects.all()
    socials_red = SocialLink.objects.all()

    total_items_cart = request.session['total_items'] #Es el número total de items en la cesta

    slug = get_object_or_404(Slug, slug=slug)
    qs = Product.objects.filter(slug=slug)
    if qs.exists():
        product = qs[0]
        first_image = product.product_images.all().first().get_img()
        images = product.product_images.all()
        product_name = product.name
        
        return render(request, "Ecommerce/detail.html", locals())   
    
    qs = Category.objects.filter(slug=slug)
    if qs.exists():
        category = qs[0] #contiene la categoria
        product_and_images = Product.objects.filter(category=category.id)
        category_name = category.name
        cat = True
        return render(request, "CMS/index.html", locals())

def add_product_cart(request):
    print("algo")


