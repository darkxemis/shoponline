# Generated by Django 2.2 on 2019-10-29 23:30

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Ecommerce', '0002_auto_20191029_2302'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productimage',
            options={'verbose_name': 'Picture', 'verbose_name_plural': 'gallery'},
        ),
        migrations.AlterField(
            model_name='product',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='Text'),
        ),
    ]
