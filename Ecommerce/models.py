from django.db import models
#Import para las imagenes en galleria, si queremos asociar un articulo con más de una imagen
from image_cropping import ImageRatioField 
from easy_thumbnails.files import get_thumbnailer
#Import para el campo de moneda
from djmoney.models.fields import MoneyField
from django.db import models
#Import para texto con formato APP
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from CMS.models import AbstractImage
#from django.contrib.auth.models import User
#from datetime import date
#from django.utils.crypto import get_random_string
from django.template.defaultfilters import slugify

class Slug(models.Model):
    slug = models.SlugField(verbose_name=u'URL friendly', unique=True, blank=True, max_length=255)

    def __str__(self):
        return self.slug

class SEOModel(models.Model):
    meta_title = models.CharField( max_length=140, verbose_name='Meta-Title', blank=True)
    slug = models.OneToOneField(Slug, verbose_name=u'URL friendly', blank=True, null=True, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if not self.id and self.slug_id == None:
            try:
                id = self.__class__.objects.latest("id").id + 1
            except:
                id = 1
            slug = Slug(slug=slugify(self.__str__()))
            qs = Slug.objects.filter(slug=slug)
            if qs.exists():
                slug = Slug(slug=slugify(self.__str__())+"-%s" % str(id))
            else:
                slug = Slug(slug=slugify(self.__str__()))
            slug.save()
                
            self.slug = slug
        super(SEOModel, self).save( *args, **kwargs)
        
    class Meta:
        abstract = True

class Category(SEOModel):
    name = models.CharField(max_length=25, blank=True)
    descripcion = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'Category'
        verbose_name_plural = 'Category'

class Product(SEOModel):
    name = models.CharField(max_length=50, blank=True)
    price = MoneyField(max_digits=14, decimal_places=2, default_currency='EUR')
    text = RichTextUploadingField(verbose_name='Text', null=True, blank=True)
    category = models.ForeignKey(Category, related_name='category', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ProductImage(AbstractImage):
    cropping = ImageRatioField('image', '300x300', verbose_name=u'Visible clipping') 	
    product = models.ForeignKey(Product, related_name='product_images', null=True, on_delete=models.CASCADE)  	
    def get_img(self): 		
        return get_thumbnailer(self.image).get_thumbnail({'size': (300, 300), 'box': self.cropping, 'crop': True, 'detail': True }).url 
    def get_big_img(self):
        return self.image.url