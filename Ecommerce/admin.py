from django.contrib import admin
from .models import *
from jet.admin import CompactInline
#from django.contrib import messages
from image_cropping import ImageCroppingMixin

class CategoryAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("id", "name",)
    list_editable = ("name",)

    fieldsets = (
            (u"Category data", {
                'fields': ('name', )
            }),
        )

class ProductImageInline(ImageCroppingMixin, CompactInline):
    model = ProductImage
    extra = 0


class ProductAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ("id", "name", "price",)
    list_editable = ("name", "price",)
    inlines = [ProductImageInline, ]

    fieldsets = (
            (u"Product data", {
                'fields': ('name', 'category', 'text', ('price',))
            }),
        )

admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
