from CMS import views
from django.urls import path

urlpatterns = [
    path('', views.index, name="index"),
	path('inicio/busqueda', views.busqueda, name='busqueda'), 
]