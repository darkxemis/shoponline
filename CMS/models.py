from django.db import models
from image_cropping import ImageRatioField
from django.template.defaultfilters import slugify
from easy_thumbnails.files import get_thumbnailer
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel

"""CMS"""

class AbstractImage(models.Model):
    image = models.ImageField(upload_to='gallery', max_length=500, verbose_name='Imagen (Recomendado 1200x480)')
    text = models.TextField(verbose_name='Text', blank=True, null=True)
    
    class Meta:
        abstract = True
        verbose_name = 'Picture'
        verbose_name_plural = u'gallery'

    def __str__(self):
        return u'Picture: ' + str(self.id)

class Config(models.Model):
    logo = models.ImageField(verbose_name=u"Website logo", null=True, blank=True)
    phone = models.CharField(max_length=23, default="", blank=True, verbose_name='telephone contact')
    email = models.CharField(max_length=127, default="", blank=True, verbose_name='Email contact')
    cif = models.CharField(max_length=50, verbose_name=u'CIF', default="", blank=True)
    address = models.CharField(verbose_name=u'Address', blank=True, max_length=200)
    description = models.TextField(blank=True, null=True, verbose_name=u'Description')


    legal = RichTextUploadingField(verbose_name=u'legal warning', blank=True, null=True)

    def __str__(self):
        return u"Website Configuration"

    def get_logo(self):
        return get_thumbnailer(self.logo).get_thumbnail({'size': (366, 63), 'box': self.cropping, 'crop': True, 'detail': True }).url 

    class Meta:
        verbose_name = u"Website Configuration"
        verbose_name_plural = u"Website Configuration"

SOCIAL_TYPE = (
    ('fb','Facebook'),
    ('tw','Twitter'),
    ('g+','Google+'),
    ('yt','YouTube'),
    )

class SocialLink(models.Model):
    url = models.URLField(max_length=300, verbose_name=u'URL Red Social')
    stype = models.CharField(max_length=2, verbose_name='Nombre de Red Social', choices=SOCIAL_TYPE)

    def get_faclass(self):
        if self.stype == 'fb':
            return 'fa-facebook'
        elif self.stype == 'tw':
            return 'fa-twitter'
        elif self.stype == 'g+':
            return 'fa-google-plus'
        elif self.stype == 'yt':
            return 'fa-youtube'

    def __str__(self):
        return self.stype

    class Meta:
        verbose_name = u"Link a Red Social"
        verbose_name_plural = u"RRSS"