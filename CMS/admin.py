from django.contrib import admin
from .models import *
from image_cropping import ImageCroppingMixin
from solo.admin import SingletonModelAdmin
from jet.admin import CompactInline

class ConfigAdmin(ImageCroppingMixin, SingletonModelAdmin):
	
    fieldsets = (
            (u"General data", {
                'fields': ('logo', ('phone', 'email'), 'address', 'cif', 'description', )
            }),

        )

admin.site.register(Config, ConfigAdmin)
admin.site.register(SocialLink)