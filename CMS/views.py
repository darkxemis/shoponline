from django.shortcuts import render
from CMS.models import *

from Ecommerce.models import Category, Product, ProductImage
from django.http import HttpResponse, JsonResponse


def index(request):
	""" 1. Hago las consultas necesarias """
	categories = Category.objects.all()
	#product_and_images = Product.objects.all().prefetch_related('product_images').order_by('product_name')
	product_and_images = Product.objects.all().order_by('name')
	
	""" 2. Genero las variables necesarias """
	request.session['currency'] = '€'
	#Si en la sessión del navegador es nueva o otra, inicializaremos a 0 el carrito
	if 'cart_items' not in request.session:
		request.session['cart_items'] = [{
				'total_price': '0',
                'total_items': '0'
			}]
		request.session['total_price'] = 0
		request.session['total_items'] = 0

	total_items_cart = request.session['total_items'] #Es el número total de items en la cesta

	""" 3. Renderizo el template con mis variables """
	return render(request, 'CMS/index.html', locals())

def busqueda(request):
	if request.is_ajax(): 
		articuloimagen = Articulo.objects.filter(title__startswith=request.GET['nombre']).values('title', 'id').order_by('-title')
		json_articulos_list = list(articuloimagen) 
		
		i = 0
		#Creación del json por cada resultado obtenido en la query anterior
		for articulo_json in json_articulos_list:
			
			articulo = Articulo.objects.filter(id=articulo_json['id'])
			precio = articulo[0].precio

			id_articulo_foto = Articulo.objects.filter(id=articulo_json['id']).values_list('imagenes_articulos', flat=True).first()
			
			if ArticuloImagen.objects.filter(id=int(id_articulo_foto)).values('image').first():
				path_imagen = ArticuloImagen.objects.filter(id=int(id_articulo_foto)).values('image').first()
				json_articulos_list[i]['imagen'] = path_imagen
			else:
				json_articulos_list[i]['imagen'] = broken_img

		
			json_articulos_list[i]['precio'] = str(precio)

			i = i + 1
	
		print(json_articulos_list)
		return HttpResponse( json.dumps(json_articulos_list), content_type='application/json' )
		#return render(request, 'busqueda.html', locals())
	elif request.method == 'GET':
		print(request.GET['category_list'])
		print(request.GET['search'])
		return redirect('index')